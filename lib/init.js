const { promisify } = require('util');
const figlet = require('figlet');
const clear = require('clear');
const chalk = require('chalk');
const { spawn } = require('child_process');
const ora = require('ora');
const open = require('open');
const {clone}= require('./download.js')

const promiseFiglet = promisify(figlet);
export const redLog = (content) => console.log(chalk.red(content));
export const blueLog = (content) => console.log(chalk.blue(content));
export const greenLog = (content) => console.log(chalk.green(content));

const promiseSpawn = (...params) => {
    return new Promise((resolve) => {
    
        const proc=spawn(...params)
        proc.stdout.pipe(process.stdout);
        proc.stderr.pipe(process.stderr);
        proc.on('close', () => {
            resolve()
        });
    })
}

module.exports = async (name) => {
    clear();
    const hy=await promiseFiglet('welcome vue-auto-router-cli');
    blueLog(hy);
    await clone('github:su37josephxia/vue-template', name);
    blueLog('模板下载成功');
    greenLog('开始安装依赖');
    const loading = ora(blueLog('依赖安装中...'));
    loading.start();
    await promiseSpawn(process.platform.startsWith('win') ?'npm.cmd':'npm', ['install'], { cwd: `./${name}` });
    loading.succeed(blueLog('依赖安装成功...'));
    blueLog(`👌安装完成：
        To get Start:
        ===========================
        cd ${name}
        npm run serve
        =========================== `
    );

    await promiseSpawn('npm.cmd', ['run', 'serve'], { cwd: `./${name}` });
    open('http://localhost:8080');
};
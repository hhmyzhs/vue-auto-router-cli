
const { promisify} = require('util');
const down = require('download-git-repo');
const ora = require('ora');
const {blueLog} = require('./init.js')

const downloadGitRepo = promisify(down);

module.exports.clone = async function (url, name) {
    try {
        const progress = ora(blueLog(`${url}下载中`));
        progress.start();
        await downloadGitRepo(url, name);
        progress.succeed();
    } catch (err) {
        progress.fail(blueLog(`${url}下载失败`))
    }
}
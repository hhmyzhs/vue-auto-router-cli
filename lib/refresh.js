const fs = require('fs');
const chalk = require('chalk');
const handlebars = require('handlebars');

const compile = (des,filepath,templatepath) => {
    if (fs.existsSync(templatepath)) {
        const content = fs.readFileSync(templatepath).toString();
        const res = handlebars.compile(content)(des);
        fs.writeFileSync(filepath, res);
        console.log(chalk.blue(`${filepath}创建成功`));
    }
}

module.exports = function () {
    
    const list = fs.readdirSync('./src/views').filter((v) => v !== 'Home.vue')
        .map((v) => ({ name: v.replace('.vue', '').toLowerCase(), file: v }));
    // 生成路由定义
    compile({ list }, './src/router.js', './template/router.js.hbs');

    //生成菜单
    compile({ list }, './src/App.vue', './template/App.vue.hbs');
}